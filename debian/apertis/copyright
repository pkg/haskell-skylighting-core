Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 2016-2018, John MacFarlane.
License: BSD-3-clause

Files: debian/*
Copyright: heldthe contributors mentioned in debian/changelog
License: BSD-3-clause

Files: debian/control
Copyright: heldthe contributors mentioned in debian/changelog
License: GPL

Files: skylighting-core.cabal
Copyright: 2016-2024, John MacFarlane
License: GPL

Files: test/default.theme
Copyright: no-info-found
License: Expat

Files: xml/bash.xml
Copyright: 2004, Wilbert Berendsen (wilbert@kde.nl)
License: LGPL

Files: xml/clojure.xml
Copyright: 2004, Dominik Haumann, 2011,Caspar Hasenclever
License: LGPL-2+

Files: xml/cmake.xml
Copyright: 2013-2015, 2017-2023, Alex Turbov <i.zaufi@gmail.com>
 2007, 2008, 2013, 2014, Matthew Woehlke <mw_triad@users.sourceforge.net>
 2005, Dominik Haumann <dhdev@gmx.de>
 2004, Alexander Neundorf <neundorf@kde.org>
License: LGPL-2

Files: xml/crystal.xml
Copyright: 2022, Gaurav Shah (gauravshah.89@gmail.com)
 2011, Miquel Sabaté (mikisabate@gmail.com)
 2008, Robin Pedersen (robinpeder@gmail.com)
 2004, Stefan Lang (langstefan@gmx.at)
 2004, Sebastian Vuorinen (sebastian dot vuorinen at helsinki dot fi)
License: LGPL-2+

Files: xml/d.xml
Copyright: kate project).
 2007, Jari-Matti Mäkelä <jmjm@iki.fi>
 2007, Aziz Köksal <aziz.koeksal@gmail.com>
 2004, Simon J Mackenzie <project.katedxml@smackoz.fastmail.fm>
License: LGPL

Files: xml/dockerfile.xml
Copyright: James Turnbull <james@lovedthanlost.net>
 2020, 2021, Alex Turbov <i.zaufi@gmail.com>
License: Expat

Files: xml/dosbat.xml
 xml/tcsh.xml
Copyright: 2006, 2009, 2010, Matthew Woehlke (mw_triad@users.sourceforge.net)
License: LGPL

Files: xml/elixir.xml
Copyright: 2016, Boris Egorov (egorov@linux.com)
 2014, Rubén Caro (ruben.caro.estevez@gmail.com)
License: LGPL-2+

Files: xml/erlang.xml
Copyright: 2007, Bill Ross <bill@emailme.net.au>
License: LGPL-2

Files: xml/gnuassembler.xml
Copyright: 2021, Waqar Ahmed
 2002, John Zaitseff
License: GPL-2

Files: xml/go.xml
Copyright: 2010, Miquel Sabaté <mikisabate@gmail.com>
License: GPL-2

Files: xml/javascript-react.xml
 xml/typescript.xml
Copyright: 2018-2020, Nibaldo González S. (nibgonz@gmail.com)
License: Expat

Files: xml/lex.xml
 xml/yacc.xml
Copyright: 2004, Jan Villat <jan.villat@net2000.ch>
License: LGPL

Files: xml/m4.xml
Copyright: 2011, Cybernetica AS ( http://www.cyber.ee/ )
License: BSD-3-clause

Files: xml/maxima.xml
Copyright: 2008, Alexey V. Beshenov <al@beshenov.ru>.
License: LGPL-2.1+

Files: xml/modula-2.xml
Copyright: 2018, Modula-2 Software Foundation
License: Expat

Files: xml/monobasic.xml
 xml/systemverilog.xml
Copyright: no-info-found
License: GPL

Files: xml/nasm.xml
Copyright: no-info-found
License: GPL-2

Files: xml/nix.xml
Copyright: 2021, Marco Rebhan <me@dblsaiko.net>
License: Expat

Files: xml/perl.xml
Copyright: 2001-2004, Anders Lund <anders@alweb.dk>
License: LGPL-2

Files: xml/prolog.xml
Copyright: 2012, Torsten Eichstädt
License: LGPL-2+

Files: xml/pure.xml
Copyright: no-info-found
License: LGPL-2+

Files: xml/r.xml
Copyright: 2006, Thomas Friedrichsmeier, Arne Henningsen, and the RKWard Team
License: GPL-2

Files: xml/relaxng.xml
 xml/xslt.xml
Copyright: no-info-found
License: LGPL

Files: xml/rhtml.xml
Copyright: 2006, Wilbert Berendsen (wilbert@kde.nl
 2006, Richard Dale (rdale@foton.es)
 2005, Chris Martin (linux@chriscodes.com
 2004, Stefan Lang (langstefan@gmx.at)
 2004, Sebastian Vuorinen (sebastian dot vuorinen at helsinki dot fi)
License: LGPL-2+

Files: xml/ruby.xml
Copyright: 2011, Miquel Sabaté (mikisabate@gmail.com)
 2008, Robin Pedersen (robinpeder@gmail.com)
 2004, Stefan Lang (langstefan@gmx.at)
 2004, Sebastian Vuorinen (sebastian dot vuorinen at helsinki dot fi)
License: LGPL-2+

Files: xml/rust.xml
Copyright: 2015, The Rust Project Developers
License: Expat

Files: xml/cpp.xml xml/gcc.xml xml/modelines.xml xml/rest.xml
Copyright: (C) 2004,2006,2008 Wilbert Berendsen <wilbert@kde.nl>
 (C) 2004-2005 Dominik Haumann <dhdev@gmx.de>
 (C) 2011 Caspar Hasenclever
 (C) 2012-2015, 2017-2018 Alex Turbov <i.zaufi@gmail.com>
 (C) 2007-2008, 2013-2014 Matthew Woehlke <mw_triad@users.sourceforge.net>
 (C) 2004 Alexander Neundorf <neundorf@kde.org>
 (C) 2007-2008 Diggory Hardy <diggory.hardy@gmail.com>
 (C) 2007 Aziz Köksal <aziz.koeksal@gmail.com>
 (C) 2007 Jari-Matti Mäkelä <jmjm@iki.fi>
 (C) 2004 Simon J Mackenzie <project.katedxml@smackoz.fastmail.fm>
 (C) 2016 Boris Egorov <egorov@linux.com>
 (C) 2014 Rubén Caro <ruben.caro.estevez@gmail.com>
 (C) 2002 John Zaitseff
 (C) 2010-2011 Miquel Sabaté <mikisabate@gmail.com>
 (C) 2001 Joseph Wenninger <jowenn@kde.org>
 (C) 2001-2004 Anders Lund <anders@alweb.dk>
 (C) 2003 Simon Huerlimann <simon.huerlimann@access.unizh.ch>
 (C) 2004 Jan Villat <jan.villat@net2000.ch>
 (C) 2008 Alexey V. Beshenov <al@beshenov.ru>
 (C) 2012 Torsten Eichstädt
 (C) 2010 Matt Williams <matt@milliams.com>
 (C) 2004 Sebastian Vuorinen <sebastian dot vuorinen at helsinki dot fi>
 (C) 2004 Stefan Lang <langstefan@gmx.at>
 (C) 2005 Chris Martin <linux@chriscodes.com>
 (C) 2006 Richard Dale <rdale@foton.es>
 (C) 2008 Robin Pedersen <robinpeder@gmail.com>
 (C) 2011 Jonathan Kolberg <bulldog98@kubuntu-de.org>
License: LGPL

Files: xml/markdown.xml
Copyright: (C) 2008 Darrin Yeager
License: GPL-2+ or BSD-3-clause

Files: xml/tcl.xml
Copyright: (C) Irsid - Arcelor Innovation R&D
 (C) 2011 Cybernetica AS
License: BSD-3-clause
